<!-- Description: this page is the login page -->
<?php session_start(); ?> <!-- starts session -->
<?php $title = "Logga in"; ?> <!-- Sets the title of the page -->
<?php include "functions.php"; ?><!-- Gives access to functions.php -->

<!doctype html>
<html lang="sv">
    <head>
       <!-- Metatags that must come first -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"><!-- IE supports a number of document compatibles modes,
		that enables different features, Edge is the current highest mode -->
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"><!-- To ensure proper rendering and touch zooming for all devices -->
        
        <link rel="stylesheet" href="css/style.css">
        
        <title><?php echo $title ?></title>
    </head>
    <body>
		<div class="main_content_container">
            <div class="login_box">
				<div class="login_content">
					<h1>Logga in</h1>
				</div><!-- .login_content -->
				<div class="login_content">
					<form method="post" action="logincheck.php">
						<label for="user_email" class= "left">Email</label>
					    <br>
						<input type="text" id="user_email" name="user_email" placeholder="Email" value="" required>
						<br>
						<label for="user_password">Lösenord</label>
						<br>
						<input type="password" id="user_password" name="user_password" placeholder="Lösenord" value="" required>
						<br>
						<input type="submit" id="login-submit" name="submit" value="Logga in">
					</form>
				</div><!-- .login_content -->
				
				<!-- Shows a message when you have logged out -->
                <?php 
                if(isset($_GET["logout"]) && $_GET["logout"] == true) {
                   set_message("Du är nu utloggad!", "green", "login.php");
                }
                ?>
				
				<!-- Using the function display_message from functions.php to display a message if wrong login information is put in,
				 message is set in logincheck.php -->
            
                <?php display_message(); ?>
				
            </div><!-- .login_box -->
        </div><!-- .main_content_container -->
    </body>
</html>