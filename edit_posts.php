<?php

// ***** Description: edit_post.php is for changing the title, the category, the picture or/and the text that belongs to a certain blog post. ****
 
session_start(); 
$title = "Redigera inlägg";
include "db_variables.inc"; 
include "nav.php";
include "functions.php";


/* This if-statement checks if there is an user who´s loged-in, if not it redirects to login page.
 This prevents not logged in users to access edit_posts page. */

if( !isset($_SESSION['logged_in']) ) {
    header("Location: login.php");
}

/* When a post title is clicked on admin.php a get-array is generated from functions.php
and passed to this file. The session variable is from the file logincheck.php */

$post_cat_id = $_GET["cat_id"]; 
$post_id = $_GET["p_id"];
$username = $_SESSION["username"];

if( isset($_GET["feedback"]) ){
    set_message("Inlägget är uppdaterat", "green", "edit_posts.php?p_id={$post_id}&cat_id={$post_cat_id}");
}

/* This block fetches all columns from table 'posts' and column cat_title 
 from table categories and put it into variables */

$query = "SELECT posts.* , categories.cat_title FROM posts 
		  LEFT JOIN categories ON '{$post_cat_id}' = categories.cat_id 
		  WHERE post_id = '{$post_id}'";

if( $stmt->prepare($query) ){
	$stmt->execute();
	$stmt->bind_result($post_id, $post_cat_id,$post_title, $post_user, $post_date, $post_image, $post_content, $category);
	$stmt->fetch();
}
?>
<article>
    <div class="main_content_container">
		<div class="edit_post_header">
			<h1> <?php echo "Redigera inlägg"; ?> </h1>
		</div>
		<?php display_message(); ?>
		<div class="edit_post_main">
			<form method="post" enctype="multipart/form-data">
		
				<!-- Selection menu for categories -->
				<label class="label_text" for="cat" > BYT KATEGORI:</label><br>
				<select name="categories" id="cat">
	                <?php echo "<option value='$post_cat_id'>{$category}</option>" ?> <!-- Selected category -->
					<option value="1">Afrika</option>
					<option value="2">Asien</option>
					<option value="3">Europa</option>
					<option value="4">Nordamerika</option>
					<option value="5">Sydamerika</option>
					<option value="6">Oceanien</option>
					<option value="7">Antarktis</option>
					<option value="8">Övrigt</option>
				</select>
				<br>
		
		
				<!-- Textarea 1: edit heading -->
				<label for="edit_heading"> REDIGERA RUBRIK: </label>
				<br>
				<textarea name="heading" id="edit_heading" rows="2" cols="60"><?php echo $post_title;?></textarea>
				
				<!-- Picture and button to choose new image file -->
				<div>		
					<img class="imgSize" src=" <?php echo $post_image; ?> " alt="Bild till inlägg"><br>
					<input type="file" name="new_image"> <br>
				</div>
		
				<!-- textarea 2: edit post -->
				<div>
					<label for="edit_post"> REDIGERA INLÄGG: </label><br>
					<textarea name="post" id="edit_post" rows="10" cols="60"><?php echo $post_content; ?></textarea>
					<br>
					<input type="submit" name="submit" value="Publicera">
				</div>

			</form>

		</div>
    </div><!-- .main_content_container -->
	</article>
<?php
/* If submit post button is pressed, the database will be updated. There are two queries: the first one if the image is changed, 
   the second one if the image is not changed. */

if( isset($_POST["submit"]) ){

	$new_post = strip_tags($_POST["post"]);
	$new_category = $_POST["categories"];
	$new_heading = strip_tags($_POST["heading"]);


	if(!empty($_FILES["new_image"]["name"]) ) {

		if( ($_FILES["new_image"]["size"]) > 5000000) {
				set_message("Bilden är för stor, max 5mb", "red", "");
                header("Location: edit_posts.php?p_id={$post_id}&cat_id={$post_cat_id}");
		 		exit;
		 	}else{
		
				// This block create a new name for picture file and checks the file type
				$time = date("YmdHis");
				$target_folder = "pics/";					                                          
				$target_name = $target_folder . basename("post-" . "$post_id" . "$time" . ".jpg");
				$type = strtolower( pathinfo($target_name, PATHINFO_EXTENSION) ); 

				if($type == "jpg" || $type == "jpeg") {
		
 					move_uploaded_file($_FILES["new_image"]["tmp_name"], $target_name); 
			
 					$query = "UPDATE posts 
						      SET post_category_id = '{$new_category}',
						      post_title = '{$new_heading}',
						      post_image = '{$target_name}', 
						      post_content = '{$new_post}'
						      WHERE post_id = $post_id  ";	
				}else{
					set_message("Fel filformat, endast jpg/jpeg-filer accepteras.", "red", "");
                    header("Location: edit_posts.php?p_id={$post_id}&cat_id={$post_cat_id}");                    
					exit;
				}
			}

	} else { 

		$query = "UPDATE posts 
			      SET post_category_id = '{$new_category}',
			      post_title = '{$new_heading}', 
			      post_content = '{$new_post}'
			      WHERE post_id = $post_id  ";

	}

	if( $stmt->prepare($query) ){
		$stmt->execute();


		header("Location: edit_posts.php?p_id=$post_id&cat_id=$new_category&feedback=ok"); 
		
	}else{ 
		 set_message("Inlägget blev inte uppdaterat", "red", "");	 
	}

}

// Closes the database connection
$conn->close();	
?>

</body>
</html>

