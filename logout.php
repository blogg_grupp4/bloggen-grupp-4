<!-- Description: this page is used for login out the user. -->
<?php
session_start();

/*
Here the sessions are unset and session logged_in is set to false, and session is destroyed.
The cookie username is removed, and user is directed to the login page.
*/

$_SESSION["logged_in"] = false;
unset($_SESSION["username"]);
unset($_SESSION["user_id"]);

session_destroy();

setcookie("username", $un, time() -3600);

header("Location: login.php?logout=true");

