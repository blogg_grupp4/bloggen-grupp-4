<div class="footer_content">
	<div class="footer_contact">
		<p class="footer_p">Telefon: 070-00 00 00</p>
		<p class="footer_p">Webbplats: <a id="bottom" href="jordenrunt">www.JordenRunt.se</a></p>
		<p class="footer_p">Email: jordenrunt@hotmail.com</p>
	</div> 

	<div class="bounce"><a href="#top"><i class="faa fa-arrow-circle-up" aria-hidden="true"></i></a></div>

	<div class="footer_follow">Följ oss här</div>
	<div class="footer_icons">
		<a href="https://twitter.com/"> <i class="fa fa-twitter-square fa-3x" aria-hidden="true"></i> </a>

		<a href="https://www.facebook.com/"> <i class="fa fa-facebook-square fa-3x" aria-hidden="true"></i> </a>

		<a href="https://www.instagram.com/"> <i class="fa fa-instagram fa-3x" aria-hidden="true"></i> </a>
	</div>
</div>
</body>
</html>
