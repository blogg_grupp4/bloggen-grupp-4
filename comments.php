<!-- Description: This page is what you see if you want to comment on a blog post. You see the blog post that you want to comment, previous comments and a form to create a new comment to the blog post. -->
<?php
	$title = "Kommentarer";
	include "db_variables.inc";
	include "header1.php";
	include "functions.php";
?>

<?php
	// från url:en
	$showComments = $_GET["posts"];

	if(isset($_POST["submit"])) {

		if( !empty($_POST["commentContent"]) && !empty($_POST["commentName"]) && !empty($_POST["commentEmail"]) ) {

			$cPostId = mysqli_real_escape_string($conn, $showComments);
			$cName = mysqli_real_escape_string($conn, $_POST["commentName"]);
			$cEmail = mysqli_real_escape_string($conn, $_POST["commentEmail"]);
			$cUrl = mysqli_real_escape_string($conn, $_POST["commentUrl"]);
			$cComment = mysqli_real_escape_string($conn, $_POST["commentContent"]);

			$query = "INSERT INTO comments (comment_post_id, comment_author, comment_email, comment_website, comment_content) VALUES ('{$cPostId}', '{$cName}', '{$cEmail}', '{$cUrl}', '{$cComment}')";
			/*
			<!-- **************************************************** -->
			<!-- ******** FEEDBACK THAT THE COMMENT WAS OKEY ******** -->
			<!-- **************************************************** --> */
			if($stmt->prepare($query)) {
				if($stmt->execute()) {

					// when you submitted a comment the page will refresh and you will see the comment you just wrote
					header("Location: comments.php?posts=$showComments&feedback=OK");
				}
			}
		} else { 
			// this message will show if the user doesn't fill in the form for the comment, name and email.
			set_message("Du måste fylla i alla fälten för kommentar, namn och epost.", "red", "");
		}
	}

	$query = "SELECT * FROM posts WHERE post_id = '{$showComments}'";
	if($stmt->prepare($query)) {
		$stmt->execute();
		$stmt->bind_result($post_id, $post_cat_id, $post_title, $post_user, $post_date, $post_image, $post_content);
		$stmt->fetch();
	}
?>

<!-- A div to push down the content that was under the header -->
<div class="push_down_content"> 

</div> <!-- push_down_content -->

<div class="comment_post">
	<article>
		<!-- **************************************************** -->
		<!--   HERE WE ECHOING OUT THE POST WITH DATE AND TITLE   -->
		<!-- **************************************************** -->

		<div class="comment_box">
			<h2><?php echo $post_title ?></h2>
			
			<p><?php echo substr($post_date, 0, -8) ?></p> 				<!-- substr is used to show the date without the seconds  -->
			<p><?php echo $post_content ?></p>
		</div> <!-- comment_box -->


		<!-- **************************************************** -->
		<!-- ***** HERE WE ECHO OUT THE COMMENTS TO THE POST **** -->
		<!-- **************************************************** -->
		<div class="center_comments">
			<h3>Tidigare kommentarer</h3>
			<?php
				$query = "SELECT comment_author, comment_email, comment_content, comment_date FROM comments LEFT JOIN posts ON comments.comment_post_id = posts.post_id WHERE posts.post_id = '{$post_id}'";
				if($stmt->prepare($query)) {
					$stmt->execute();
					$stmt->bind_result($commentAuthor, $commentWebsite, $commentToPost, $commentDate);
				}

						while( mysqli_stmt_fetch($stmt) ) {
							?> 
							<!-- This div makes space between the prevous comments that is to the blog post -->
							<div class="comment_space_between">

								<p class="comment_author_date"><?php
									echo "$commentAuthor, ";
									echo substr($commentDate, 0, -8)."<br>"; ?> 
								</p>

								<?php
									echo "$commentToPost<br>";
								?>

							</div> <!-- comment_space_between -->
							<?php
						}

					// if there is 0 rows in the database, then the blog post dont have any comments 
					// then this message will show
					if($stmt->num_rows === 0) {
						echo "<p>Ingen har kommenterat detta inlägg. Skriv den första kommentaren!</p>";
					}
			?> 
		</div> <!-- center_comments -->
		
		<?php
			// this message will show 
			if(isset($_GET['feedback'])) {
                
				set_message("Kommentaren har lagts till!", "green", "comments.php?posts=$showComments");
            
			}
		?>
		
		<div class="main_content_container">
            <div class="comments_display_message">
	            <?php display_message(); ?><!-- function display_message displays a message -->
            </div><!-- comments_display_message -->
        </div><!-- .main_content_container -->

		<!-- **************************************************** -->
		<!-- ********** THE FORM TO LEAVE A COMMENT ************* -->
		<!-- **************************************************** -->
		<div class="center_comments">
			<h3>Kommentera</h3>
			<form method="post"> 			<!-- The area where you write the comments to the blog post  -->
				<textarea name="commentContent" cols="50" rows="10"></textarea><br>
				<label>namn</label><br>
				<input type="text" name="commentName" placeholder="Skriv ditt namn här"><br>
				<label>e-post</label><br>
				<input type="text" name="commentEmail" placeholder="Skriv din e-post här"><br>
				<label>webbsida</label><br>
				<input type="text" name="commentUrl" placeholder="Skriv din webbsida här"><br>
				<input type="submit" name="submit" value="Lämna kommentar">
			</form>
		</div>
		<?php 
			$conn->close();
		?>
	</article>
</div> <!-- index_blog_post -->
<?php
	include "footer.php";
?>