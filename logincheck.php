<!-- Description: this page checks the inputed information from login page. And if its correct sends user to admin page. -->
<?php 
session_start();
include "functions.php";
include "db_variables.inc";// give access to database connection thru db_variables.inc

/* 
Check if login button is pressed and check so user email and password fields are not empty,
escapes special characters. Does a query and stores database column values into variables.

Check so userid is not 0 and compares passwords with built in function password_verify(verifies that password matches hash).
If values are correct it sets a cookie named username with logged in users username as value, it last for 8h.
 
Sets 4 session values, logged_in to true, username to username, user_id to user_id and post_user to post_user, 
all these are values from the database for current user.
 
If succeeded user get to admin.php otherwise a message is shown for user and the user remains on login page 
*/

if(isset($_POST["submit"])) {

	if( !empty($_POST["user_email"]) && !empty($_POST["user_password"]) ) {

		$user_email = mysqli_real_escape_string($conn, $_POST["user_email"]);
		$user_password = mysqli_real_escape_string($conn, $_POST["user_password"]);

		$stmt = $conn->stmt_init();

		if($stmt->prepare("SELECT * FROM users WHERE user_email = '{$user_email}' ")) {
			$stmt->execute();

			$stmt->bind_result($id, $un, $up, $uf, $ul, $ue);
			$stmt->fetch();
            
            $result = password_verify($user_password, $up);

			if($id != 0 && $up == $result) {

				setcookie("username", $un, time() + (3600 * 8));
                
                $_SESSION["logged_in"] = true;
                $_SESSION["username"] = $un;
                $_SESSION["user_id"] = $id;
                $_SESSION["post_user"] = $uf;
                
                header("Location: admin.php?login=true");
                
            } else {
                set_message("Fel inloggnings uppgifter!", "red", "");
                header("Location: login.php?login=false");

			}

		}

	}

}