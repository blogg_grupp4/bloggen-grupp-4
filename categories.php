
<!-- ************  Description: This is where you land when a category link is clicked. 
				   All posts of the chosen category is looped out in descending order ************* -->

<?php

$title = "Kategorier";
include "db_variables.inc";
include "header1.php";


// From index.php
$post_category_id = $_GET["catId"];
$cat_name = $_GET["category"];


// Query to get all columns from table posts that matches with the choosen category
$query = "SELECT posts.*, users.username FROM posts 
		  LEFT JOIN users ON posts.post_user = users.user_id 
		  WHERE posts.post_category_id = '{$post_category_id}' 
		  ORDER BY post_date DESC";

// Prepare, execute and binds database content into variables
if( $stmt->prepare($query) ){
	$stmt->execute();
	$stmt->bind_result($post_id, $post_cat_id,$post_title, $post_user, $post_date, $post_image, $post_content, $username);
}
?>
<div class="push_down_content"></div>

		
			<div class="index_blog_post"> 
				<article>

					<?php	
					// Loop which fetches the variables from above with content from database	    
					while( mysqli_stmt_fetch($stmt) ){
					?>
					
					<!-- This is where the blog post prints out with date/time, title, author, category, comment link, 
					image and text -->
					<time> <?php echo substr($post_date, 0, -8); ?> </time>
					<h1> <?php echo $post_title; ?> </h1>
				
					<ul>
						<li><a href="#"> Skrivet av: <?php echo " " . $username; ?> </a></li>
						<li><a href="#"> Kategori: <?php echo $cat_name; ?> </a></li>
						<li><a href="comments.php?posts=<?php echo $post_id ?>"> Kommentarer </a></li>      
					</ul>
				
					<img src=" <?php echo $post_image; ?>" alt="Bild till inlägg '<?php echo $post_title ?>' ">
					<div>
						<p> <?php echo $post_content; ?> </p>
					</div>
					<?php
					}
					$conn->close(); // Closes the database connection
					?>

				</article>
			</div>
		
<?php
include "footer.php";
?>
