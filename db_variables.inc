<?php 

$server = "localhost";
$username = "root";
$password = "";
$database = "blogg";

// Connect to database
$conn = new mysqli($server, $username, $password, $database);
if( $conn->connect_errno ){
		die("Ett fel har uppstått: " . $conn->connect_error);	
	}

// Sets the default client character set
mysqli_set_charset($conn,"utf8");

// Create a prepared statement
$stmt=$conn->stmt_init();

?>

