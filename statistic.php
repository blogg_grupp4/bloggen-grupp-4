<!-- Description: On this page we count the number of comments and blog posts that we hade in the database -->
<?php session_start(); ?>

<?php 
  $title = "Statistik";
  include "nav.php";

  if(!isset($_SESSION['logged_in'])) {
    
    header("Location: ./login.php");
  }
?>

  <div class="create_post_header">
    <h1>Översikt på statistiken:</h1>
  </div>
  <div class="admin_content">
    <!-- Number of blog posts -->
    <?php
        include "db_variables.inc";

            $stmt = $conn->stmt_init();
            $query = "SELECT COUNT(post_id) FROM posts";         // count how many post_ids it is in the database
            if($stmt->prepare($query)) {
                $stmt->execute();
                $stmt->bind_result($post_count);
                $stmt->fetch();
            }
    ?>
    <p>Totalt antal blogginlägg är:</p>
    <p class="statistics_numbers"><?php echo "$post_count"; ?> st.</p>

    <!-- Number of comments -->
    <?php
        $stmt = $conn->stmt_init();
        $query = "SELECT COUNT(comment_id) FROM comments";      // count how many comment_ids it is in the database
        
        if($stmt->prepare($query)) {
          $stmt->execute();
          $stmt->bind_result($comment_count);
    			$stmt->fetch();
        }
    ?>
    <p>Totalt antal kommentarer är:</p>
    <p class="statistics_numbers"><?php echo "$comment_count"; ?> st.</p>

    <!-- How many comments there is avarage of every blog post -->
    <?php

      $average = $comment_count / $post_count;                 // here we divide the number of comments and the number of post ids

    ?>
    <p>Totalt antal kommentarer i genomsnitt per blogginlägg är: </p>
    <p class="statistics_numbers"><?php echo round($average, 1); ?>  st.</p>                <!--  That gives us the avarage number  -->
  </div>

<?php                                                           // here we close the connection to the database 
    $conn->close(); 
?>

</body>
</html>
