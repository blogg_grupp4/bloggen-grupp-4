 <!-- Description: This page is the startrpage of the blogg. It gathers information from 3 different tabels posts, users and categories.
 and displays the information of the blogposts on the page.  -->
<?php 
    $title = "Index";
    include 'header1.php';
?>
<?php
    include "db_variables.inc";

    // Getting information from fron 3 different tabels post, users and categories.
    $query = "SELECT posts.*, users.user_firstname, categories.cat_title
    FROM posts
    LEFT JOIN users 
    ON posts.post_user = users.user_id
    LEFT JOIN categories
    ON posts.post_category_id = categories.cat_id ORDER BY post_date DESC";

    if($stmt->prepare($query)) {
        $stmt->execute();
        $stmt->bind_result($postId, $postCategory, $postTitle, $postUser, $postDate, $postImage, $postContent, $userName, $catName);
    }
?>

      
<div class="index_blog_post"> 
    <article>
       <?php
            while( mysqli_stmt_fetch($stmt) ){
                ?>
                <time> <?php echo substr($postDate, 0, -8); ?> </time>
                <h1> <?php echo $postTitle; ?> </h1>
        
                <ul>
                    <li><a href="#"> Skrivet av: <?php echo " " . $userName; ?> </a></li>
                    <li><a href="categories.php?category=<?php echo $catName ?>&catId=<?php echo $postCategory ?> "> Kategori: <?php        echo $catName; ?> </a></li>
                    <li><a href="comments.php?posts=<?php echo $postId ?>"> Kommentarer </a></li>
                </ul>
        
                <img src="<?php echo $postImage; ?>" alt="Bild till inlägg"> 
                <div>
                    <p> <?php echo $postContent; ?> </p>
                </div>
        
            <?php
            }
            ?>     
    </article>
</div>
      
<?php
    include 'footer.php'; 
?>