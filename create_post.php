<!-- Description: This page is where you create a new blogpost for the blog. And it saves to the database and will show on the index page when you click on the submit button. -->
<?php session_start(); ?>

<?php $user_id = $_SESSION["user_id"]; ?>

<?php
    $title = "Skapa nytt inlägg";
    include "nav.php";
    include "db_variables.inc";
	include "functions.php";

    if(!isset($_SESSION['logged_in'])) {
    
    header("Location: ./login.php");
    }

    // function to save the chosen category if you get a message 
    function SelectCategory($category){

        if(isset($_POST["category"]) && ($_POST["category"]) == $category) {
        echo "selected";
        }
    }
?>

<?php
    if(isset($_POST["submit"])) {              // if you click the submit button named upload this below will happen: 

        if( !empty($_POST["post_title"]) && !empty($_POST["post_content"]) && !empty($_POST["category"]) ) {

            $post_title = mysqli_real_escape_string($conn, $_POST["post_title"]);
            $post_content = mysqli_real_escape_string($conn, $_POST["post_content"]);
            $post_category_id = mysqli_real_escape_string($conn, $_POST["category"]);

            $stmt = $conn->stmt_init();             // The information that you want to post is inserted to the database 
            $query = "INSERT INTO posts (post_title, post_content, post_category_id, post_user) VALUES ('{$post_title}', '{$post_content}', '{$post_category_id}', '{$user_id}')";

            if($stmt->prepare($query)) {            // you will get a message if the upload succeded
                if($stmt->execute()) {
                    set_message("Inlägget har publicerats", "green", "");
                    unset($_POST);                  // flushes textfields after you have posted a blog post
                } 

                $error = $_FILES["post_picture"]["error"];
                if($error == UPLOAD_ERR_OK) {

                    $time = date("YmdHis");
                    $post_id = $conn->insert_id;
                    $target_folder = "pics/";           // here we insert the picture into a folder called pics
                    $target_name = $target_folder . basename("post-".$post_id.$time.".jpg");

                    if(move_uploaded_file($_FILES["post_picture"]["tmp_name"], $target_name)) {

                        $query = "UPDATE posts SET post_image = '{$target_name}' WHERE post_id = '{$post_id}'";
                        $stmt = $conn->stmt_init();     // and the name of the picture is saved in the database under post_image

                        if($stmt->prepare($query)) {
                            $stmt->execute();

                        } else {
                            echo mysqli_error();
                        } 
                    } else {                            // if the upload for some reason did not work, this message will show
                        set_message("Något är fel!", "red", "");
                    }
                }
            } 
        } else {
            set_message("Du måste fylla i fälten för <br>rubrik, inlägg och välja kategori", "red", "");
        }
    }
?>

<div class="main_content_container">
	<div class="create_post_header">
		<h1>Skapa nytt inlägg:</h1>
	</div>
	<!-- <div class="create_post">  -->   
    <div class="edit_post_main"> 
		<form class="form_padding" method="post" enctype="multipart/form-data">           <!-- The start of the form to create a new post  -->
			<label for="rubrik">Rubrik</label><br>
			<textarea name="post_title" id="rubrik"><?php 

			// to save the content in the textarea even if you get a message
			if(isset($_POST["post_title"])) {
				echo $_POST["post_title"];
			}
			?></textarea><br>

			<label for="in">Inlägg</label><br>
			<textarea name="post_content" id="in"><?php 

			// to save the content in the textarea even if you get a message
			if(isset($_POST["post_content"])) {
				echo $_POST["post_content"];
			}
			?></textarea> <!-- The area where you write the post -->

			<br>
			<label for="postImg">Lägg till bild:</label><br>                        <!-- Insert a picture to the post -->
			<input type="file" id="postImg" name="post_picture">

			<br>
			<label for="postCat">Välj en kategori: </label><br>                     <!-- Choose a category to the post -->
			<select name="category" id="postCat">
				<option value="0">Välj kategori</option>
				<option value="1"<?php SelectCategory(1); ?> >Afrika</option>
				<option value="2"<?php SelectCategory(2); ?>>Asien</option>
				<option value="3"<?php SelectCategory(3); ?>>Europa</option>
				<option value="4"<?php SelectCategory(4); ?>>Nordamerika</option>
				<option value="5"<?php SelectCategory(5); ?>>Sydamerika</option>
				<option value="6"<?php SelectCategory(6); ?>>Oceanien</option>
				<option value="7"<?php SelectCategory(7); ?>>Antarktis</option>
				<option value="8"<?php SelectCategory(8); ?>>Övrigt</option>
			</select>
			<br>
			<input type="submit" name="submit" value="Ladda upp">
		</form>                                                         <!-- End of the form  -->
	</div>
	
	 <?php display_message(); ?><!-- function display_message displays a message that post as been deleted. -->
	
</div>

<?php                                                           // here we close the connection to the database 
    $conn->close(); 
?>

</body>
</html>