<!-- Description: this is the header for all pages in the blogg. If you are logged in you can se "Admin"" and "Logga ut"-->
<!DOCTYPE html>
<html lang="sv">
<head>
	<title> <?php echo $title ?> </title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta charset=utf-8>
</head>
<body>
	<header>
		<a id="top"></a>		
		<?php session_start(); ?>

		<?php
			$class = "";
			if(strpos(strtolower($_SERVER['PHP_SELF']), "index.php") !== FALSE) {
				$class = "background_header";
			}
		?>

		<div class="<?php echo $class; ?>">
			<nav>
				
				<div class="header">
					<h1 class="header_title">JORDENRUNTBLOGGEN</h1>
					<div class="links_index">
						<ul>
							<li><a class="link_header" href="index.php">Hem</a></li>
							<li><a class="link_header" href="#">Om oss</a></li>
							<li><a class="link_header" href="#bottom">Kontakt</a></li>
							<?php
								if(isset($_SESSION["logged_in"])) {
							?>
							<li><a class="link_header" href="admin.php">Admin</a></li>
							<li><a class="link_header" href="logout.php">Logga ut</a></li>
							<?php
								}
							?>	
						</ul>
					</div>
				</div>				
			</nav>
		</div>
	</header>
