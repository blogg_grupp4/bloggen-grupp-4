<!DOCTYPE html>
<html>
<head>
	<title> <?php echo $title ?> </title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<meta name="viewport" content="width=device-width, initial-scale=1" />
</head>
<body>

<!-- This is the navigation meny for the admin pages. It allows the logged in user to navigate between all the pages -->
	<nav>
		<div class="links_admin">
			<ul>
				<li><a href="index.php">Hem</a></li>
				<li><a href="admin.php">Admin</a></li>
				<li><a href="create_post.php">Nytt inlägg</a></li>
				<li><a href="edit_user.php">Redigera användare</a></li>
				<li><a href="statistic.php">Statistik</a></li>
				<li><a href="logout.php">Logga ut</a></li>
			</ul>
		</div>
	</nav>