<!-- Description: this page allow a user to edit their information, such as username, password, email,  user first and lastname  -->
<?php session_start(); ?>
<?php $title = "Redigera användare"; ?>
<?php include "nav.php"; ?>
<?php include "db_variables.inc"; ?>
<?php include "functions.php"; ?>

<!-- This if statement checks if session logged_in exists, if not it redirects to login page.
 This prevents not logged in users to access edit user page. -->
<?php 

if(!isset($_SESSION['logged_in'])) {
    
    header("Location: login.php");
}

?>
	<div class="main_content_container">
		<div class="create_post_header">
			<h1>Redigera användare</h1>
		</div>
		<div class="create_post">
			<?php
			
			/* 	
			Uses the session "user_id", which is set in loggincheck.php,
			todo a query to get the logged in users information. 
			Uses a while loop to get the information.
			*/

			$the_user_id = $_SESSION["user_id"];

			$query = "SELECT * FROM users WHERE user_id = '{$the_user_id}' ";
			$select_users_query = mysqli_query($conn, $query);

			confirmQuery($select_users_query);

			while($row = mysqli_fetch_assoc($select_users_query)) {

				$user_id            = $row['user_id'];
				$username           = $row['username'];
				$user_password      = $row['user_password'];
				$user_firstname     = $row['user_firstname'];
				$user_lastname      = $row['user_lastname'];
				$user_email         = $row['user_email'];

			}
			
			/*  
			Checks if "Redigera uppgifter" button is pressed and then uses function clean from functions.php
			to convert html code into HTML entities.
			
			Then it uses the function escape from functions.php
			to escape special characters and remove whitespace before and after input.
			*/

			if(isset($_POST['submit'])) {
				
				$user_firstname     = clean($_POST['user_firstname']);
				$user_lastname      = clean($_POST['user_lastname']);
				$username           = clean($_POST['username']);
				$user_email         = clean($_POST['user_email']);
				$user_password      = clean($_POST['user_password']);
				
				$user_firstname     = escape($user_firstname);
				$user_lastname      = escape($user_lastname);
				$username           = escape($username);
				$user_email         = escape($user_email);
				$user_password      = escape($user_password);
				
				/* 
				Checks if user input "Lösenord" is not empty and then run the code inside the if statement.
				
				Runs a query which gets the users password from database, uses the function confirmQuery from functions.php
				to give user a message if query fails. 
				
				Then checks if password from database is same as the userinput password,
				if not it hashes the password, if password matches it don't do anyting. 
				
				Last it does a query to insert updated
				information into database and uses confirmQuery again, and tells user that information has been updated.
				*/
				
				if(!empty($user_password)) { 
					
					$query_password = "SELECT user_password FROM users WHERE user_id = 
					$the_user_id";
					$get_user_query = mysqli_query($conn, $query_password);
					confirmQuery($get_user_query);

					$row = mysqli_fetch_array($get_user_query);

					$db_user_password = $row['user_password'];


					if($db_user_password != $user_password) {

						$hashed_password = password_hash($user_password, PASSWORD_DEFAULT);

						$user_password = $hashed_password;

					  }


						$query = "UPDATE users SET ";
						$query .="user_firstname  = '{$user_firstname}', ";
						$query .="user_lastname = '{$user_lastname}', ";
						$query .="username = '{$username}', ";
						$query .="user_email = '{$user_email}', ";
						$query .="user_password   = '{$user_password}' ";
						$query .= "WHERE user_id = {$the_user_id} ";


						$edit_user_query = mysqli_query($conn,$query);

						confirmQuery($edit_user_query);

						set_message("Användareuppgifterna uppdaterade", "green", "");
				} 

			} 
				
			$conn->close(); // closes connection to database
			
			?>
			
			<!-- Form to display users current information and give user ability to change information. -->

			<form action="edit_user.php" method="post" enctype="multipart/form-data">    
				<div>
					<label for="post_firstname">Förnamn</label>
					<br>
					<input type="text" id="post_firstname" value="<?php echo $user_firstname ?>" name="user_firstname">
				</div>
				
			   <div>
					<label for="post_lastname">Efternamn</label>
					<br>
					<input type="text" id="post_lastname" value="<?php echo $user_lastname ?>" name="user_lastname">
				</div>
				
				 <div>
					<label for="post_username">Användarnamn</label>
					<br>
					<input type="text" id="post_username" value="<?php echo $username ?>" name="username">
				</div>
				
				<div>
					<label for="post_email">Email</label>
					<br>
					<input type="email" id="post_email" value="<?php echo $user_email ?>" name="user_email">
				</div>
				
				<div>
					<label for="post_password">Lösenord</label>
					<br>
					<input type="password" id="post_password" value="<?php echo $user_password ?>" name="user_password">
				</div>
				 
				<div>
					<input type="submit" name="submit" value="Redigera uppgifterna">
				</div>
			</form>
		</div><!-- .create_post -->
		<?php display_message(); ?>
	</div><!-- .main_content_container -->
</body>
</html>
