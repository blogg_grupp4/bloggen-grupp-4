<!-- Description: this is the admin page where the logged in user can do administration tasks.  -->
<?php session_start(); ?><!-- starts session -->
<?php $title = "Admin"; ?>
<?php include "nav.php";?><!-- includes navigation to page. -->
<?php include "db_variables.inc"; ?><!-- give access to database connection thru db_variables.inc -->
<?php include "functions.php"; ?><!-- gives access to functions.php -->

<!-- This if statement checks if session logged_in exists, if not it redirects to login page.
This prevents not logged in users to access admin page. -->

<?php

if(!isset($_SESSION['logged_in'])) {
    
    header("Location: ./login.php");
}

?>

	<div class="main_content_container">
		<div class="admin_content">
			<h1>
			   Välkommen <?php echo $_SESSION['username']; ?> <!-- welcomes the user by displaying session username -->
			</h1>
		 </div><!-- .admin_content -->
		<div class="admin_content">
			<table class="post_table">
				<caption>Dina blogginlägg</caption>
				<thead>
					<tr>
						<th>Titel</th>
						<th>Datum</th>
						<th></th>
						<th></th>
					</tr>
				</thead>
				<tbody>

				<?php show_posts(); ?><!-- function show_posts(from functions.php)displays the logged in users blogg posts -->

				</tbody>
			</table>
		</div><!-- .admin_content -->
    
	<?php delete_post(); ?><!-- function delete_post deletes bloggpost -->
    
	<?php display_message(); ?><!-- function display_message displays a message that post as been deleted. -->
	
	<!-- A message is set to go into function display_message.-->
    <?php
    if(isset($_GET['delete'])) {

		set_message("Blogginlägget raderat", "red", "");
    
    }
    ?>

     </div><!-- .main_content_container -->
</body>
</html>