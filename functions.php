<?php

/* This page contains all functions created for the project */

/**************************************************
                DB FUNCTIONS
***************************************************/

/* 
Function which confirms the query to the databas, if anything fails it exit the script and displays that
together with the output of built in function mysqli_error message. 
Function have access to $conn which has to be global since $conn is defined outside the function.
*/

function confirmQuery($result) {

    global $conn;
    
    if(!$result) {

    die("QUERY FAILED ." . mysqli_error($conn));

    }

}

/* 
The function is used to escape special characters on the incoming $string and returns it escaped.
Function have access to $conn which has to be global since $conn is defined outside the function. Trim removes whitespace before and after input
*/

function escape($string) {
    
    global $conn;
    
    return mysqli_real_escape_string($conn, trim($string));
    
}

/**************************************************
                HELPER FUNCTIONS
***************************************************/

/* 
The function convert html code into HTML entities on input content.
*/

function clean($string) {
    
    return htmlentities($string);
    
}

/**************************************************
                ADMIN SHOW POSTS
***************************************************/

/*
Function have access to $conn which has to be global since $conn is defined outside the function.
Function saves session "username" into a variable, it then uses it in a query to the database to get the logged in user bloggposts
so it can be displayed. It uses a while loop to echo out the blog posts title and date on the screen. 
Links to edit post and delete post are created at the same time. In the edit post link, post_id and post_category_id is sent whith it for use on edit_post.
*/

function show_posts() {
    
    global $conn;
    
    $post_logged_in_user = $_SESSION["username"];

    $query = "SELECT posts.*, users.username FROM posts LEFT JOIN users ON posts.post_user = users.user_id WHERE username = '{$post_logged_in_user}' "; 
        
    $select_posts_query = mysqli_query($conn, $query);
        
    confirmQuery($select_posts_query);

    while($row = mysqli_fetch_assoc($select_posts_query)) {

        $post_id            = $row['post_id'];
		$post_category_id   = $row['post_category_id'];
        $post_title         = $row['post_title'];
        $post_date          = $row['post_date'];
		
		$post_date          = substr($post_date, 0, -9);
		
		$_SESSION["post_category_id"] = $post_category_id;
        $_SESSION["post_id"] = $post_id;

        echo "<tr>";
        echo "<td>{$post_title}</td>";
        echo "<td>{$post_date}</td>";
        echo "<td><a href='edit_posts.php?p_id={$post_id}&cat_id={$post_category_id}'>Redigera</a></td>";
        echo "<td><a href='admin.php?delete={$post_id}'>Ta bort</a></td>";
        echo "</tr>";
        
    }
    
}

/* 
Function have access to $conn which has to be global since $conn is defined outside the function. 
It checks if $_GET has the value 'delete', delete link is clicked, then it stores that into a variable with the posts id.
Then a query to delete the post is done and the page is "refreshed" thru header function to update page with removed post gone.
*/

function delete_post() {
    
    global $conn;
    
    if(isset($_GET['delete'])) {

    $the_post_id = $_GET['delete'];

    $query = "DELETE FROM posts WHERE post_id = {$the_post_id} ";
    $delete_query = mysqli_query($conn, $query);

    confirmQuery($delete_query);
    
    header("Location: admin.php");    
        
    }
    
}

/**************************************************
              MESSAGE FUNCTIONS
***************************************************/

/* 
This function takes in the variable $message, which contains the value put into it from the page which uses this function.
If $message is not empty and contains data it sets a session whith value 'message' otherwise variable $message its set to contain no data.
*/

function set_message($message, $color, $link) {
    
    if(!empty($message)) {
		
		if($color == "green") {
            
            $color = "feedback_green";
            
        } else {
            
            $color = "feedback_red";
            
        }
        
        $message2 = "<div class='popup'>
                        <div class='content'>
                        <a href='$link'>&times;</a>
                        <p class='$color'> $message </p>
                        </div>
                    </div>";
        
        $_SESSION['message'] = $message2;
        
    }else {
        
        $message = "";
        
    }
    
}

/* 
This function displays message set in set_message function. it checks if session 'message' exists, then displays it and last it unset the session.
*/

function display_message() {
    
    if(isset($_SESSION['message'])) {
        
        echo $_SESSION['message'];
        
        unset($_SESSION['message']);
        
    }
    
}

/**************************************************
               UPCOMING FUNCTIONS
***************************************************/